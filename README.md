**STRATIFY** is a comprehensive **MATLAB** package for calculating basic electromagnetic properties of a general stratified (multilayered) sphere. Its detailed description with underlying theory is freely available in [OSA Continuum](https://www.osapublishing.org/osac/abstract.cfm?uri=osac-3-8-2290)

The latest [release](https://gitlab.com/iliarasskazov/stratify/-/releases/v1.1) includes:

* absorption, scattering and extinction
* radiative and nonradiative decay rates of an electric dipole emitter located in any nonabsorbing shell, including host
* electric and magnetic near-fields in any shell, including host
* electric and magnetic energy density and total energy

The package contains the following folders with routines:

*    *bessel* for the Bessel and Riccati-Bessel functions
*    *decay* for spontaneous decay rates
*    *energy* for electromagnetic energy
*    *field* for near-fileds and far-field properties
*    *materials* for refractive indices 
*    *scripts* for examples of using the package
*    *util* for transfer matrices and electron free path correction

**References**

[1] [I. L. Rasskazov, P. S. Carney, and A. Moroz, "STRATIFY: a comprehensive and versatile MATLAB code for a multilayered sphere," OSA Continuum 3 (8), 2290-2306 (2020)](https://www.osapublishing.org/osac/abstract.cfm?uri=osac-3-8-2290)

[2] [A. Moroz, “A recursive transfer-matrix solution for a dipole radiating inside and outside a stratified sphere,” Annals Phys. 315, 352–418 (2005)](https://www.sciencedirect.com/science/article/pii/S0003491604001277)

[3] [I. L. Rasskazov, A. Moroz and P. S. Carney, “Electromagnetic energy in multilayered spherical particles,” J. Opt. Soc. Am. A 36 (9), 1591-1601 (2019)](https://www.osapublishing.org/josaa/abstract.cfm?uri=josaa-36-9-1591)

**Acknowledgements**

We thank Lorenzo Pattelli for useful comments and bug reports.